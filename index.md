---
layout: default
title: Our Attacks on ApplePay-Visa & Visa-Level1  
---

## News -- January 2023

* Our Apple-Visa attack is still LIVE (!!!), sadly. See us demo-ing it very much in  real-life, on stage, here: [over-the-limit-payment, live, from locked iPhone](https://www.youtube.com/watch?v=nMIMxLbSE-4)

            
## Summary of findings


* The Apple Pay lock screen can be bypassed for any iPhone with a Visa card set up in transit mode. The contactless limit can also be bypassed allowing unlimited EMV contactless transactions from a locked iPhone.
  
* An attacker only needs a stolen, powered on iPhone. The transactions could also be relayed from an iPhone inside someones bag, without their knowledge. The attacker needs no assistance from the merchant and  
  backend fraud detection checks have not stopped any of our test payments. 
  
* Below we include a video demo and further down a video of a 1000 pound payment being taken from a locked iPhone to a standard shop EMV reader. 

* This attack is made possible by a combination of flaws in both Apple Pay and Visa's system. It does not, for instance, affect Mastercard on Apple Pay or Visa on Samsung Pay. 

* Our work includes formal modelling that shows that either Apple or Visa could mitigate this attack on their own. We informed them both months ago but neither have fixed their system, so the vulnerability remains live.
    
* We recommend that all iPhone users check that they do not have a Visa card set up in transit mode, and if they do they should disable it.  

## Demo Video

This video explaining all the pieces of equipment involved in the attack and how they
interact. The video contains an edit that protects personal banking information.

<div class="demo">
<video controls playsinline preload="auto" width="100%" >
	<source src="assets/relay_explained.mp4" type="video/mp4" />
    <source src="assets/relay_explained.webm" type="video/webm" />
	Your browser does not support the video tag.
</video>
</div>

## Details


Contactless Europay, Mastercard, and Visa
([EMV](https://en.wikipedia.org/wiki/EMV)) payments are a fast and easy way to
make payments and are increasingly becoming a standard way to pay. However, if
payments can be made with no user input, this increases the attack surface for
adversaries and especially for relay attackers, who can ferry messages between
cards and readers without the owner's knowledge, enabling fraudulent payments.
Payments via smart-phone apps generally have to be confirmed by a user via a
fingerprint, PIN code, or Face ID. This makes relay attacks less of a threat.

However, Apple Pay introduced the "Express Transit/Travel" feature (May 2019)
that allows Apple Pay to be used at a transport-ticketing barrier station
without unlocking the phone, for usability purposes. We show that this feature
can be leveraged to bypass the Apple Pay lock screen, and illicitly pay from a
locked iPhone, using a Visa card, to any EMV reader, for any amount, without
user authorisation.

Furthermore, Visa has proposed a protocol to stop such relay attacks for cards.
We show that Visa's proposed relay-countermeasure can be bypassed using a pair
of [NFC](https://en.wikipedia.org/wiki/Near-field_communication)-enabled Android
smart phones, of which one is rooted.

We propose a new relay-resistance protocol, <span class="l1rp">L1RP</span>,
based on our findings that EMV distance bounding can be done more reliability at
Level 1 (ISO 14443-A), than Level 3 (EMV application). We formally verify our
<span class="l1rp">L1RP</span> protocol, and prove it secure using
[Tamarin](https://tamarin-prover.github.io/).

## Apple Pay Transport Mode Attack Explained

The attack against Apple Pay Transport mode is an active
[Man-in-the-Middle](https://en.wikipedia.org/wiki/Man-in-the-middle_attack)
replay and [relay attack](https://en.wikipedia.org/wiki/Relay_attack). It
requires an iPhone to have a Visa card (credit or debit) setup as "transport
card".

If a non-standard sequence of bytes (_Magic Bytes_) preceeds the standard ISO
14443-A WakeUp command, Apple Pay will consider this a transaction with a
_transport_ EMV reader.

<img src="{{ base_url }} assets/setup_apple.png" class="diagram" />

We use a [Proxmark](https://proxmark.com/) (this will act as a reader emulator)
to communicate with the victim's iPhone and an NFC-enabled Android phone (which
acts as a card emulator) to communicate with a payment terminal. The Proxmark
and card emulator need to communicate with each other. In our experiements, we
connected the Proxmark to a laptop, to which it communicated via USB; the laptop
then relayed messages to the card emulator via WiFi. The Proxmark can also
directly communicate with an Android phone via Bluetooth. The Android phone
does not require rooting.

The attack requires close proximity to the victim's iPhone. This can be achieved
by holding the terminal emulator near the iPhone, while its rightful owner is
still in posession, by stealing it or by finding a lost phone.

The attack works by first _replaying_ the _Magic Bytes_ to the iPhone, such that
it believes the transaction is happening with a transport EMV reader. Secondly,
while _relaying_ the EMV messages, the Terminal Transaction Qualifiers (TTQ),
sent by the EMV terminal, need to be modified such that the bits (flags) for
_Offline Data Authentication (ODA) for Online Authorizations supported_ and _EMV
mode supported_ are set. Offline data authentication for online transactions is
a feature used in special-purpose readers, such as transit system entry gates,
where EMV readers may have intermittent connectivity and online processing of a
transaction cannot always take place. These modifications are sufficient to
allow relaying a transaction to a non-transport EMV reader, if the transaction
is under the contactless limit.

In order to relay transactions over the contactless limit, the Card Transaction
Qualifiers (CTQ), sent by the iPhone, need to be modified such that the bit
(flag) for _Consumer Device Cardholder Verification Method_ is set. This tricks
the EMV reader into believing that on-device user authentication has been
performed (e.g. by fingerprint). The CTQ value appears in _two_ messages sent by
the iPhone and _must_ be changed in both occurances.

A video of the relay in action taking  _£1000_ from a locked iPhone:

<div class="demo">
<video controls playsinline preload="auto" width="100%" >
	<source src="assets/apple_pay_visa.mp4" type="video/mp4" />
    <source src="assets/apple_pay_visa.webm" type="video/webm" />
	Your browser does not support the video tag.
</video>
</div>




### Responsible Disclosure

The details of this vulnerability have been disclosed to Apple (Oct 2020) and to
Visa (May 2021). Both parties acknowledge the seriousness of the vulnerability,
but have not come to an agreement on which party should implement a fix.

### What Can a User Do to Protect Themselves?

While either Visa or Apple implement a fix for the problem, we recommend users
to **not** use Visa as a transport card in Apple Pay. If your iPhone is lost or
stolen, activate the [Lost
Mode](https://support.apple.com/en-gb/guide/icloud/mmfc0f0165/icloud) on your
iPhone, and call your bank to block your card.

## Visa-L1 Attack Explained

Our second attack (unrelated to Apple Pay) is against Visa's proposed protection
against relay attacks, which we dubbed the Visa-L1 protocol. This attack is
independed of our Apple Pay work. Visa-L1 relies on the inability of the
attacker to change the UID of a card or mobile phone and the difficulty of
relaying the ISO 14443 messages, due to their timing constraints. However,
setting a desired UID on some mobile devices is
[possible](https://smartlockpicking.com/slides/Confidence A 2018 Practical
    Guide To Hacking RFID NFC.pdf), if the device is rooted.

Briefly, in Visa-L1, the UID the card sends to the EMV reader, at ISO 14443
level, is then resent, encrypted, as part of the EMV protocol. The EMV reader
will decrypt it, and check if it matches the initial UID.

<img src="{{ base_url }} assets/setup_visal1.png" class="diagram" />

An attack can be carried out with a pair of NFC-enabled Android phones, _one_ of
which needs to be rooted (the phone acting as the card emulator). The reader
emulator reads the UID of the Visa-L1 card and sends this to the card emulator,
which sets it as the device's UID. A straightforward relay of EMV messages is
then carried out.

The attack is possible because the protocol's security relies on a random value
send _only_ from the card side, which we can manipulate, and there is no
randomness from the _EMV reader_.

### Responsible Disclosure

The details of this vulnerability have been discussed with Visa. The protocol is
meant to protect against attackers using unmodified devices, and Visa believe
that rooting an Android smartphone is a _difficult_ process, which requires high
technical expertise.

### What Can a User Do to Protect Themselves?

The Visa-L1 protocol is, to the best of our knowledge, not yet implemented in
commercial cards, therefore users should not be affected.


### Selected media

Our Apple Pay + Visa work has been widely reported in the media. Below is a 
selected subset of the articles available:

* [BBC -- Researchers find Apple Pay, Visa contactless hack](https://www.bbc.co.uk/news/technology-58719891)
* [The Independent -- Remove Visa from Apple Pay travel card feature due to dangerous flaw, experts say](https://www.independent.co.uk/life-style/gadgets-and-tech/apple-pay-visa-travel-security-b1929750.html)
* [The Times -- Apple Pay users 'vulnerable to security hack'](https://www.thetimes.co.uk/article/apple-pay-users-vulnerable-to-security-hack-2q3l08wbp)
* [Telegraph -- Apple Pay flaw risks letting hackers drain money from iPhones](https://www.telegraph.co.uk/technology/2021/09/30/apple-pay-flaw-risks-letting-hackers-drain-money-iphones/)
* [Sky News -- Apple Pay users should not set Visa as transport payment card, security experts warn](https://news.sky.com/story/apple-pay-users-should-not-use-visa-as-transport-payment-card-security-experts-warn-12421336)
* [ZDNet -- Researchers discover bypass 'bug' in iPhone Apple Pay, Visa to make contactless payments](https://www.zdnet.com/article/researchers-discover-bypass-bug-in-iphone-visa-apple-pay-to-make-contactless-payments/)
* [Tom's guide -- Your Apple Pay payments can be stolen over the air -- here's what to do](https://www.tomsguide.com/uk/news/apple-pay-replay-attack)
* [BleepingComputer -- Apple Pay with VISA lets hackers force payments on locked iPhones](https://www.bleepingcomputer.com/news/security/apple-pay-with-visa-lets-hackers-force-payments-on-locked-iphones/)
* [HackerNews](https://news.ycombinator.com/item?id=28711807)


### Contacts

<div class="contacts">
    <div>
        <a href="https://inaradu.com">Andreea-Ina Radu</a><br />
        <a href="https://www.cs.bham.ac.uk/~tpc/">Tom Chothia</a><br /><br /><br />
        <a href="https://www.birmingham.ac.uk/index.aspx">University of Birmingham</a>
    </div>
    <div>
        <a href="https://www.surrey.ac.uk/people/ioana-boureanu">Ioana Boureanu</a><br />
        <a href="https://www.surrey.ac.uk/people/christopher-j-p-newton">Christopher J.P. Newton</a><br />
        <a href="https://www.surrey.ac.uk/people/liqun-chen">Liqun Chen</a><br /><br />
        <a href="https://www.surrey.ac.uk/">University of Surrey</a>
    </div>
</div>

#### Acknowledgements
This work is part of the "TimeTrust" project, funded the UK’s National Cyber
Security Centre (NCSC). We thank Mastercard UK and Visa Research for providing
useful insights and feedback.

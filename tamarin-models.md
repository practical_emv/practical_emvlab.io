---
layout: default
title: Tamarin Models
---

Below is a brief summary of the formal models used in our paper, done using
the [Tamarin](https://tamarin-prover.github.io/) Prover.

[Mobile Visa Model](https://gitlab.com/practical_emv/tamarin-models/-/blob/main/OldMobile_Visa.spthy) -- 
Formal model for Visa transactions done via  payment apps, in normal as well as transport mode (both Apple Pay, Samsung Pay are modelled).

[Mobile Mastercard Model](https://gitlab.com/practical_emv/tamarin-models/-/blob/main/Mobile_Mastercard.spthy) --
Formal model for Mastercard transactions done via  payment apps, in normal as well as transport mode (both Apple Pay, Samsung Pay are modelled).


[Modified Basin Visa Model](https://gitlab.com/practical_emv/tamarin-models/-/blob/main/Visa_EMV_High_BasinEtAl.spthy) --
An updated model for Visa transactions done via a (plastic) card, with more details around the [IAD] and its checks; it proves that
checking the format of the
[IAD](https://www.emvco.com/wp-content/uploads/2017/05/EMV_v4.3_Book_3_Application_Specification_20120607062110791.pdf)
for Visa cards would in fact detect a previous contactless limit bypass attack proposed
in [The EMV Standard: Break, Fix, Verify](https://arxiv.org/abs/2006.08249).

[`L1RP` Model](https://gitlab.com/practical_emv/tamarin-models/-/blob/main/L1RP.spthy) --
Formal model proving the security of our new proposed protocol, `L1RP`.

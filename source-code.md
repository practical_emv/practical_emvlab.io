---
layout: default
title: Source Code
---

We make available all artifacts for our research. Below is a brief description
of the repositories. Please see each repo's `README.md` for more details
details.

[Tamarin Models](https://gitlab.com/practical_emv/tamarin-models)  -- contains
  the formal models for verifying the L1RP protocol described in the paper, as
  well as the models for the mobile versions of Visa/Mastercard.

[L1RP
  implementation](https://gitlab.com/practical_emv/l1rp_proxmark_implementation)
  -- source code for Proxmark firmware and client which implements our new
  proposed protocol, `L1RP`, via two extra commands (`hf 14a noncerdr` and `hf
  14a noncesim`).

[Timing Data](https://gitlab.com/practical_emv/timing-data) -- contains the raw
data and processing scripts for the Level 1 and Level 3 timings discussed in the
paper.


### Relays

[NFCProxy](https://gitlab.com/relays/nfcproxy) -- the starting-point for our
relay-based Android Apps were the `CardEmulator` app and the `TerminalEmulator`
apps in [[1]](https://www.cs.bham.ac.uk/~tpc/Papers/CCS2020.pdf), which are in
turn based on code from
[[2]](https://www.cs.bham.ac.uk/~tpc/Papers/stoppingRelays.pdf).

[NFCProxy_uid](https://gitlab.com/relays/nfcproxy_uid) -- our modified Android
    Apps, for performing our attack against Visa's relay-protection protocol.
    
[Proxmark transport
    firmware](https://gitlab.com/relays/proxmark_transport_firmware) -- source
    code for Proxmark firmware and client, which contains an extra command `hf
    14a tfl` acting as a Transport for London barriers reader.

[RRP Relay](https://gitlab.com/relays/rrp_relay) -- implements the replay of
APDUs to a Mastercard RRP test card.

[Servers](https://gitlab.com/relays/server) -- folder containing all the python
    scripts needed to run the different servers for our relay apps.

[Traces](https://gitlab.com/relays/traces) -- proxmark traces obtained from our
    relays (et = express transit; no CDCVM).


[1] Ioana Boureanu, Tom Chothia, Alexandre Debant, Stephanie Delaune, "Security
Analysis and Implementation of Relay-Resistant Contactless Payments", at the
27th ACM Conference on Computer and Communications Security (ACM CCS), 2020.

[2] Tom Chothia et al. "Relay cost bounding for contactless EMV payments."
International Conference on Financial Cryptography and Data Security. Springer,
Berlin, Heidelberg, 2015.
